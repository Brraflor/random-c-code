//
//  numsort.c
//  
//
//  Created by Brian Flores on 3/7/15.
//
//

#include <assert.h>
#include <libgen.h>
#include <stdio.h>
#include <stdlib.h>

double inssort(const void *this, const void *that)
{
    double *thisdouble = (double*) this;
    double *thatdouble = (double*) that;
    
    return (*thisdouble > *thatdouble)
    - (*thisdouble < *thatdouble);
}

void print_array(double *array, size_t nelem)
{
    for(size_t i = 0; i < nelem; i++)
    {
        printf("%20.15g\n", array[i]);
    }
}

int main(void)
{
    int count = 0;
    double array[6] = {100,2,3,-567788,5,6};
    //
    
    for(;;)
    {
        double number;
        int scancount = scanf("%lg", &number);
        
        if(scancount == EOF)
        {//might need to change this
            printf("EOF");
            break;
        }else if(scancount == 1)
        {
            //array[count] = number;
            count++;
            //array[] = {-1,24556,-456,4566,123456,-4,0};
        }else
        {//come back to this
            printf("Error");
            break;
        }
    }
    //call for inssort & print
    const size_t nelem = sizeof array / sizeof *array;
    
    qsort(array, nelem, sizeof *array, inssort);
    print_array(array, nelem);
    //printf("%d", count);
    
    return EXIT_SUCCESS;
    
}