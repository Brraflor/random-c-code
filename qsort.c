#include <stdio.h>
#include <stdlib.h>

void printarray (char *label, int *array, size_t nelem) {
    for (size_t itor = 0; itor < nelem; ++itor) {
        printf ("%s[%2ld]=%12d\n", label, itor, array[itor]);
    }
}

int intcmp (const void *this, const void *that) {
    int *thisint = (int*) this;
    int *thatint = (int*) that;
    return (*thisint > *thatint) - (*thisint < *thatint);
}

int main (int argc, char **argv) {
    printf ("Running %s with %c args\n", *argv, argc);
    
    int array[] = {10, 72, -55, 6, 99, 42, -21, 3, 38, 29, 68, -41, };
    const size_t nelem = sizeof array / sizeof *array;
    
    printarray ("Unsorted", array, nelem);
    qsort (array, nelem, sizeof *array, intcmp);
    printarray ("Sorted", array, nelem);
    
    return EXIT_SUCCESS;
}
